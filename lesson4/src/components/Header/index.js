import React from 'react';
import '../../bootstrap.min.css';
import '../Header/header.css';
import Radium from 'radium';

class Header extends React.Component {
    state={
        menu:[
            {
                lable: "Home",
                link:"/"
            },
            {
                lable: "About",
                link:"/About"
            },
            {
                lable: "News",
                link:"/News"
            },
        ]
    }
    render() {
        const style={
            fontSize:'50px',
            // border:'1px solid red',
            textDecoration:'none',
            color:'black',
            fontWeight: '900',
            ':hover':{
                color:'red',
                cursor:'pointer'
            }        
        }
        return (
            <div className="container-fluid">
                <div className="container">
                    <header>
                        <nav>
                            <ul>
                            {   this.state.menu.length !== 0 ?
                                this.state.menu.map((i)=>{
                                    return (<li><a key={i.lable} href={i.link} style={style} >{i.lable}</a></li>)
                                })
                                : <h1>Menu not find</h1>
                            }
                            </ul>
                        </nav>
                    </header>
                </div>
            </div>
        )
    };
};
export default Radium(Header)
// btn "Show"/"Hide"  on click books from state
//content+ Aside