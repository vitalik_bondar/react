import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/Header';
// import radium from 'radium';
import './index.css';

class Main extends React.Component{
    render(){
        return(
            <div>
                <Header />                
            </div>
        );
    }
}

ReactDOM.render(<Main />, document.getElementById('root'));
