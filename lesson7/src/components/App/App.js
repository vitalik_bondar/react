import React from 'react';
import CreateReactClass from 'create-react-class';
import '../../bootstrap.min.css';
import TextArea from '../../components/TextArea/TextArea';
import AppList from '../AppList/AppList';
import './App.css';

// const arr = [
//     {
//         id: 3,
//         name: 'name3',
//         text: 'text3',
//         color: 'red'
//     },
//     {
//         id: 2,
//         name: 'name2',
//         text: 'text2',
//         color: 'blue'
//     },
//     {
//         id: 1,
//         name: 'name1',
//         text: 'text1',
//         color: 'green'
//     },
// ];

const App = CreateReactClass({
    getInitialState() {
        return {
            Items: [],
        };

    },
    updateStorage: function(){
        let note=JSON.stringify(this.state.Items);
        localStorage.setItem('Items',note);
    },
    componentDidMount(){
        this.setState({
            Items: JSON.parse(localStorage.getItem('Items'))
        })
    },
    componentDidUpdate(){
        this.updateStorage();
    },
    handlerAddNewNote: function (note){
        let newNotes=this.state.Items.slice();
        newNotes.unshift(note);
        this.setState({
            Items : newNotes
        });
        console.log(newNotes);
    },

    handlerDeleteItem:function(id){
        let arr=this.state.Items;
        let idNorm =id+1;
        console.log(id);
        arr.splice(arr.findIndex(i=> i.id === idNorm),1);
        // for(let e=1;e<arr.length;e++){
        //     if(arr[e].id === idNorm){
        //         alert(e);
        //         alert(arr[e].id);
        //         arr.splice(e,1);
        //     }
        // }

        // alert(arr.findIndex(i=> i.id === id));
        this.setState({
            Items : arr
        });
        console.log(arr);
    },
    render() {
        return (
            <div>
                <div className='container-fluid'>
                    <div className='container mainContent'>
                        <TextArea onAddNotes={this.handlerAddNewNote}/>
                        <AppList notes={this.state.Items} delItem={this.handlerDeleteItem}/>
                    </div>
                </div>
            </div>
        );
    }
});

export default App;