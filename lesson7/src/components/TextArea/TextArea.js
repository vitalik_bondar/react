import React from 'react';
import CreateReactClass from 'create-react-class';
import '../../bootstrap.min.css';
import './textarea.css';

// let counter = 1;
const TextArea = CreateReactClass({
    getInitialState () {
        return{
            text:'',
            err: ''
        }
      },
      handlerChange:function(event){
        this.setState({
            text:event.target.value
        })
      },
      handlerAdd:function(){
        //   alert('dsds');
        if(this.state.text){
            let newNote={
                text:this.state.text,
                color:`#${Math.floor((Math.random()*1000)/1)}`,
                id: Date.now(),
                name:'dsd'
            };
            this.props.onAddNotes(newNote);
            // counter++;
            // alert('ok');
        };
        if(this.state.text===''){
            this.setState({
                err:'Sorry can\'t add empty note'
            })
        } else {
            this.setState({
                err:''
            })
        }
      },
    

    // getNote: function() {

    //     const note = {
    //         id: counter,
    //         name: `user${counter}`,
    //         text:this.refs.note.value,
    //         color: `#${Math.floor((Math.random()*1000)/1)}`
    //     };
    //     this.props.onAddNotes(note);

    //     counter++;
    // },
    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-10'>
                        <textarea placeholder='...' ref={'note'} onChange={this.handlerChange}></textarea>
                    </div>
                    <div className='col-2 btnText'>
                        <input type='button' value='Press me!' onClick={this.handlerAdd} />
                    </div>
                
                </div>
                {this.state.err}
            </div>
        );
    }
});
export default TextArea;