import React from 'react';
import CreateReactClass from 'create-react-class';
import '../../bootstrap.min.css';
import './item.css';

const Item = CreateReactClass({
    // getInitialState () {
    //     console.log('Item getinit');
    //   },
    deleteItem: function(){
        this.props.delItem(this.props.id-1);
    },
   render(){
       return(
           <div style={{backgroundColor:this.props.color}} className='item'>
               <div className='id'>
                    {this.props.id}                    
                </div>
                <div className='name'>
                    {this.props.name}
                </div>
                <div className='text'>
                    {this.props.text}
                </div>
               <input type="button" onClick={this.deleteItem} value='delete'/>
           </div>
       );
   }
});
export default Item;