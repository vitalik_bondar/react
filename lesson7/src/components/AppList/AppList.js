import React from 'react';
import CreateReactClass from 'create-react-class';
import Item from '../Item/Item';
import '../../bootstrap.min.css';

const AppList = CreateReactClass({
    // getInitialState () {
        
    //     console.log('applist getinit');
    //   },
   render(){
       return(
           <div>
               {this.props.notes.map((e) => {
                   return <Item key={e.id} id={e.id} name={e.name} text={e.text} color={e.color} delItem={this.props.delItem}/>
               })}
           </div>
       );
   }
});
export default AppList;