import React from 'react';
import ReactDOM from 'react-dom';
import Main from './Components/Main';
import './index.css';
import Reducer from './Components/Reducer';
// import Redux from 'redux';
import {Provider} from 'react-redux';
import {createStore} from 'redux';

const store= createStore(Reducer);
store.subscribe(()=>{console.log('store', store.getState())});

const main =(
    <Provider store={store}>
        <Main />
    </Provider>
)



ReactDOM.render(main, document.getElementById('root'));

