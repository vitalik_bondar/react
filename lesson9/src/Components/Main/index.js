import React from 'react';
import {connect} from 'react-redux';
class Main extends React.Component{   
    render() {
        console.log(this.props);
      return (
        <div>
          <h2>Counter</h2>
          <div>
            <button onClick={this.props.decrement}>-</button>
            <span>{this.props.count}</span>
            <button onClick={this.props.increment}>+</button>
          </div>
        </div>
      )
    }
  }
 const mapStateToProps =(state) =>{
    return{
        count:state.count
    }
}
const mapDispatchToProps=(dispatch)=>{
    return{
        onIncrement:()=>dispatch({type:'increment'}),
        onDecrement:()=>dispatch({type:'decrement'})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Main);