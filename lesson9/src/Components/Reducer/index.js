const InitState={
    count:0
}
export default function Reducer (state=InitState,action){
    switch (action.type) {
        case 'increment':
          return state.count + 1
        case 'decrement':
          return state.count - 1
        default:
          return state.count
      }    
}