import React from 'react';
import One from '../src/components/One/One';
import Some from '../src/components/Some/Some';
import About from './components/About/About';
import NotFound from '../src/components/NotFound/NotFound';
import { Route, Switch, NavLink } from "react-router-dom";
import '../src/bootstrap.min.css';

class Router extends React.Component {

    render() {
        return (

            <div className='container'>
                <ul>
                    <li>
                        <NavLink to="/about" activeStyle={{
                            fontWeight: "bold",
                            color: "red"
                        }}>About</NavLink>
                    </li>
                    <li>
                        <NavLink to="/some" activeStyle={{
                            fontWeight: "bold",
                            color: "red"
                        }}>Some</NavLink>
                    </li>
                    <li>
                        <NavLink to="/one" activeStyle={{
                            fontWeight: "bold",
                            color: "red"
                        }}>Blog</NavLink>
                    </li>
                </ul>

                <hr />
                <Switch>
                    <Route path="/about" component={About} />
                    <Route path="/some" exact component={Some} />
                    <Route path="/one" component={One} />
                    <Route component={NotFound} />
                </Switch>

            </div>

        )
    };
};
export default Router
