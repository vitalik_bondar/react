import React from 'react';
import BlogItem from '../BlogItem/BlogItem';
class BlogList extends React.Component{
    render(){
        return(
            <div>
                <button>home</button>
                {this.props.items.map((e) => {
                    return <BlogItem {...this.props} key={e.id} id={e.id} author={e.author} text={e.text} date={e.date} getId={this.props.getId}/>                   
               })}
            </div>
        )
    };
};
export default BlogList