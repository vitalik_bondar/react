import React from 'react';
class BlogItem extends React.Component{
    handlerGetItemsId =(Id)=>{
        let id = this.props.id;
        this.props.getId(id);
    }
    render(){
        console.log(this.props);        
        return(
            <div onClick={this.handlerGetItemsId}>
                <div className='id'>
                    {this.props.id}                    
                </div>
                <div className='author'>
                    {this.props.author}
                </div>
                <div className='text'>
                    {this.props.text}
                </div>
                <div className='date'>
                    {this.props.date}
                </div>
            </div>
        )
    };
};
export default BlogItem