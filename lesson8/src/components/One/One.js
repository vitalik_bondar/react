import React from 'react';
import BlogList from '../BlogList/BlogList';
class One extends React.Component{
    state = {
        items:[
            {
                id: '1',
                author: 'kenny',
                text: 'Some text',
                date : '2018',
                status: ''
            },
            {
                id: '2',
                author: 'Tom',
                text: 'Hello my name is Tom',
                date : '2019',
                status: ''
            },
            {
                id: '3',
                author: 'John',
                text: 'xxx',
                date : '2019',
                status: ''
            },
        ]

    };
    handlerGetId =(Id)=>{
        let newUrl = `One/${Id}`;
        console.log(`One/${Id}`); 
               
    }
    render(){
        return(
            <div>
                <BlogList items={this.state.items} getId={this.handlerGetId}/>
            </div>
        )
    };
};
export default One