const Redux = require('redux');
console.log('Hello world');
const startState = {
    counter: 0
};
const increment = {
    type:'increment'
};
const decrement = {
    type:'decrement'
}
const reducer=(state=startState,action)=>{
    console.log('reducer');
    // switch (action.type) {
    //     case 'increment':
    //       return counter= state.counter + 1;
    //     case 'decrement':
    //       return counter= state.counter - 1;
    //     default:
    //       return counter=state.counter;
    //   }
    if (action.type=='increment'){
        return{
            counter:state.counter+1
        }
    } ;
    if (action.type=='decrement'){
        return{
            counter:state.counter-1
        }
    } 
    // else return state.counter;
    
};
const Valera= Redux.createStore(reducer);
Valera.subscribe(()=>{console.log('store', Valera.getState())});

// console.log('increment', store.getState());
Valera.dispatch(increment);
Valera.dispatch(increment);
Valera.dispatch(increment);
Valera.dispatch(decrement);
Valera.dispatch(increment);
Valera.dispatch(increment);
Valera.dispatch(increment);
// console.log('decrement',store.getState());
