import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';



import Books from './components/books';

class Main extends React.Component {
    render() {
        console.log('render.Main.Component')
        return (
            <div>
                <Books />
            </div>
        )
    }

}

ReactDOM.render(<Main />, document.getElementById('root'));
