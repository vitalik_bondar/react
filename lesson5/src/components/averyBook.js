import React from 'react';
import Style from './books.scss';
class EveryBook extends React.Component {

    componentWillMount() {
        console.log('componentWillMount');
    }
    componentDidMount() {
        console.log('componentDidMount');
    }
    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps', nextProps);
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log('shouldComponentUpdate', nextProps, nextState);
       if (nextState.price > 2000)
        {
            return true;
        }
        else{
            console.log('regect')
            return false;
        }

    }
    state = {
        price: 0
    }
    PriceHendler = () => {
        this.setState({price: this.refs.myPrice.value})
    }
    render() {


        console.log('render Every Book')

        return (<div>
            <div className='wrap'>
                <div className='Title'>{this.props.title}</div>
                <div className='Autor'>{this.props.autor}</div>
                <div className='text'>{this.props.text}</div>
                <div className='Publ'>{this.props.publish}</div>
                <div> Price: {this.state.price}</div>
                <input type ='text' ref='myPrice' />
                <button type='button' onClick = {this.PriceHendler} > Button</button>
            </div><br /></div>
        )
    }
}
export default EveryBook;