import React, { Component } from 'react';
import EveryBook from "./averyBook";
import Style from './books.scss';





class Books extends Component {
    state = {
        books: [
            {
                title: 'Good Book',
                autor: "Ole Lukoe",
                text: "В прошлом году я написал небольшую книгу об изучении React.js, которая заняла примерно 100 страниц. В этом году я собираюсь бросить вызов самому себе и сжать ее до размера статьи.",
                publish: '11.14.2002',
            },
            // {
            //     title: 'God delusion',
            //     autor: "Dowkinz",
            //     text: "В прошлом году я написал небольшую книгу об изучении React.js, которая заняла примерно 100 страниц. В этом году я собираюсь бросить вызов самому себе и сжать ее до размера статьи.",
            //     publish: '11.14.1995',
            // },
            // {
            //     title: 'Liliput',
            //     autor: " Bolli  Milli",
            //     text: "В прошлом году я написал небольшую книгу об изучении React.js, которая заняла примерно 100 страниц. В этом году я собираюсь бросить вызов самому себе и сжать ее до размера статьи.",
            //     publish: '08.2.2002',
            // }
        ]
    }
    render() {

        console.log('render.Books.Component')
        return (
            <div>
                {this.state.books.length !== 0 ?
                    this.state.books.map(item => {
                        return (  
                            <EveryBook key={item.title} title= {item.title} autor={item.autor} text={item.text} publish={item.publish} />
                        )
                        
                    })
                    : <div>Books not found</div>
            } 
            </div>
        )
    }

}
export default Books;