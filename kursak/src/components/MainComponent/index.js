import React from 'react';
import MenuList from '../MenuList';
// import { connect } from 'react-redux';

class MainComponent extends React.Component{
    render(){
        // console.log(this.props);
        return(
            <div>
                <MenuList />
            </div>
        )
    }
};

// const mapStateToProps = (state) =>{
//     // console.log(state);
//     return {
//         name:state
//     };
// };
// export default connect(mapStateToProps)(MainComponent);
export default MainComponent;