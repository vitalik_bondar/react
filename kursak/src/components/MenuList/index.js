import React from 'react';
import { connect } from 'react-redux';
import MenuItem from '../MenuItem';

class MenuList extends React.Component{
render(){
    console.log(this.props);
    return (
        <div>
            {this.props.menu.map((item)=>{
               return <MenuItem key = {item.id} Name={item.name} Ingradients={item.ingredients} Amount={item.amount} Price={`$${item.price}`} />
            })}
        </div>
    );
}
}
const mapStateToProps = (state) =>{
    // console.log(state);
    return {
        menu:state.itemMenu
    };
};
export default connect(mapStateToProps)(MenuList);