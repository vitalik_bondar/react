import {plus,plus2,plus5,minus,minus2,minus5} from './constants';
export const PLUS = () =>{
    return{
        type:plus
    }
}
export const PLUS2 = () =>{
    return{
        type:plus2
    }
}
export const PLUS5 = () =>{
    return{
        type:plus5
    }
}
export const MINUS = () =>{
    return{
        type:minus
    }
}
export const MINUS2 = () =>{
    return{
        type:minus2
    }
}
export const MINUS5 = () =>{
    return{
        type:minus5
    }
}