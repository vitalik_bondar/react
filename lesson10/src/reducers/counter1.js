
import {plus,minus} from '../constants';
const initState = {
    counter: 0,
}
export default function reduser(state = initState, action) {
    switch (action.type) {
        case plus:
            return {
                counter: state.counter + 1
            };
            break;
        case minus:
            return {
                counter: state.counter - 1
            }
            break;

        default:
            return state
            break;
    }
}
