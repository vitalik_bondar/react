

const initState = {
    counter5: 0,
}
export default function reduser(state = initState, action) {
    switch (action.type) {
        case 'plus5':
            return {
                counter5: state.counter5 + 5
            }
            break;
        case 'minus5':
            return {
                counter5: state.counter5 - 5
            }
            break;
        default:
            return state
            break;
    }
}
