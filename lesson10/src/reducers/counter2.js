

const initState = {
    counter2: 0,
}
export default function reduser(state = initState, action) {
    switch (action.type) {
        case 'plus2':
            return {
                counter2: state.counter2 + 2
            }
            break;
        case 'minus2':
            return {
                counter2: state.counter2 - 2
            }
            break;
        default:
            return state
            break;
    }
}
