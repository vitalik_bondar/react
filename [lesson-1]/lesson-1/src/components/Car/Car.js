import React from 'react';
import '../Car/car.css';
class Car extends React.Component{   
    render(){
        const prop=this.props;
        const baseStyle={
            'textAlign':'center',
            'color':'red',
            'fontStyle':'italic',
            'padding':'20px'
        }
        return(
            <div style={baseStyle}>
                <div>Model:{prop.Model}</div>
                <div>Type:{prop.Type}</div>
                <div className='Engine'>Engine:{prop.Engine}</div>
                <div>Color:{prop.Color}</div>
                <div>Price:{prop.Price}</div>
            </div>
        )
    };
};
export default Car