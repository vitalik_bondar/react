import React from 'react';
import Car from '../Car/Car'
class Header extends React.Component{
    render(){
        return(
            <div>
               Content.js
               <Car Model={'BMW'} Type={'Sedan'} Engine={2.4} Color={'Grey'} Price={10000} />
               <Car Model={'Ford'} Type={'Sedan'} Engine={1.6} Color={'White'} Price={7000} />
               <Car Model={'Audi'} Type={'Sedan'} Engine={2.6} Color={'Black'} Price={10000} />
            </div>
        )
    };
};
export default Header