import React from 'react';
import ReactDOM from 'react-dom';
import ContactList from './components/ContactList/ContactList';
import './index.css';


ReactDOM.render(<ContactList />, document.getElementById('root'));
