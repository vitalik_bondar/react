import React from 'react';
import CreateReactClass from 'create-react-class';
import '../../components/ContactList/contacts.css';
import '../../bootstrap.min.css';

const list=[
    {
        name : 'Batman',
        description : 'The Dark Knight of Gotham City begins his war on crime with his first major enemy being the ' +
        'clownishly homicidal Joker.',
        director : 'Lviv',
        image : 'https://media.giphy.com/media/a5viI92PAF89q/giphy.gif'
    },
    {
        name : 'Terminator',
        description : 'A seemingly indestructible android is sent from 2029 to 1984 to assassinate a waitress, whose' +
        ' unborn son will lead humanity in a war against the machines, while a soldier from that war is sent to protect' +
        ' her at all costs.',
        director : 'James Cameron',
        image : 'https://media.giphy.com/media/YjfAfZyzEoOZi/giphy.gif'
    },
    {
        name : 'Spiderman',
        description : 'When bitten by a genetically modified spider, a nerdy, shy, and awkward high school student' +
        ' gains spider-like abilities that he eventually must use to fight evil as a superhero after tragedy ' +
        'befalls his family.',
        director : 'Sam Raimi',
        image : 'https://media.giphy.com/media/WirhZMBF1AZVK/giphy.gif'

    },
    {
        name : 'Spiderman',
        description : 'When bitten by a genetically modified spider, a nerdy, shy, and awkward high school student' +
        ' gains spider-like abilities that he eventually must use to fight evil as a superhero after tragedy ' +
        'befalls his family.',
        director : 'Sam Raimi',
        image : 'https://media.giphy.com/media/WirhZMBF1AZVK/giphy.gif'

    },
    {
        name : 'Spiderman',
        description : 'When bitten by a genetically modified spider, a nerdy, shy, and awkward high school student' +
        ' gains spider-like abilities that he eventually must use to fight evil as a superhero after tragedy ' +
        'befalls his family.',
        director : 'Sam Raimi',
        image : 'https://media.giphy.com/media/WirhZMBF1AZVK/giphy.gif'

    }
];

const ContactList = CreateReactClass({
  getInitialState () {
    return {
      contacts : list,
    };
  },
  filterHandler(event){
    let search = event.target.value.toLowerCase();
    // console.log(search);
    let ContactList = list.filter(function(el) {
        var searchValue = el.name.toLowerCase();
        return searchValue.indexOf(search) !== -1;
    });
     console.log(ContactList);
     this.setState({
         contacts :ContactList,
     })
  },
  
  
  render() {
    return (
      <div>
          <div className='container-fluid'>
              <div className="container">
                  <div className="row">
                      <div className="col-4 offset-4 searchFilm">
                          <input type='text' onChange={this.filterHandler} placeholder='name of film...'/>
                      </div>
                  </div>
                  <div className="row">
                      {this.state.contacts.length !== 0 ?
                          this.state.contacts.map(item => {
                              return(
                                  <div className='col-4 filmItem'>
                                      <img src={item.image} alt={item.name} />
                                      <h1>{item.name}</h1>
                                      Description: {item.description} <br/>
                                      Director: {item.director} <br/>
                                  </div>
                              )

                          })
                          : <div>Books not found</div>
                      }
                  </div>
              </div>
          </div>
      </div>
    );
  }
});

export default ContactList;