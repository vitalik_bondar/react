import React from 'react';
import ReactDOM from 'react-dom';
import '../src/index.css';
import Cars from './components/Cars/Cars';
import InputComponents from './components/InputComponents/InputComponents';



class Main extends React.Component {
    state = {
        title: 'Main page',
        Cars: [
            {
                model: 'focus',
                type: 'dsds',
                color: 'dsds'
            },
            {
                model: 'x5',
                type: 'dsds',
                color: 'dsds'
            },
            {
                model: 'q7',
                type: 'dsds',
                color: 'dsds'
            }
        ]
    };
    titleHandler = () => {
        if (this.state.title === "Main page") {
            this.setState({ title: 'New State' });
        } else this.setState({ title: 'Main page' });
    };
    changeTitleHandler = (newState) => {
        this.setState({ title: newState })
    };
    render() {
        return (
            <div>
                <h2>{this.state.title}</h2>
                <button onClick={this.titleHandler}>Change state</button>
                {/* <Cars model={'Audi'} type={'Sedan'} color={'Black'} changeTitle={()=>{this.changeTitleHandler('Audi A6')}}/>
                <Cars model={'bmw'} type={'Sedan'} color={'yellow'} changeTitle={this.changeTitleHandler.bind(this,'bmw')}/>
                <Cars model={'ford'} type={'Sedan'} color={'black'} changeTitle={this.changeTitleHandler.bind(this,'ford')}/> */}
                {this.state.Cars.map((i) => {
                    return (
                        < Cars model={i.model} type={i.type} color={i.color} changeTitle={this.changeTitleHandler.bind(this, i.model)} />
                    )
                })}
                <hr></hr>
                <hr></hr>
                <InputComponents />
            </div>
        );
    }
}

ReactDOM.render(<Main />, document.getElementById('some'));