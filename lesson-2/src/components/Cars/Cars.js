import React from 'react';
class Car extends React.Component{   
    render(){
        const baseStyle={
            'textAlign':'center',
            'color':'red',
            'fontStyle':'italic',
            'padding':'20px'
        }
        return(
            <div style={baseStyle}>
                 <div className='model'>
                    {this.props.model}                    
                </div>
                <div className='type'>
                    {this.props.type}
                </div>
                <div className='color'>
                    {this.props.color}
                </div>
                <button onClick={this.props.changeTitle}>Get Car</button>
            </div>
        )
    };
};
export default Car