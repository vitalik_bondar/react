import React from 'react';
import Cars from '../Cars/Cars';

class InputComponents extends React.Component{   
    state={
        componentState1:'Input State',
        componentState2:'Input State',
        componentState3:'Input State',
    }
    changeTitle=(event)=>{
        console.log(event.target.value);
        this.setState({componentState1:event.target.value});
    }
    changeTitleButton=(event)=>{
        event.preventDefault();
        console.log(this.refs.myInput1.value);
        this.setState({componentState1:this.refs.myInput1.value});
        this.setState({componentState2:this.refs.myInput2.value});
        this.setState({componentState3:this.refs.myInput3.value});

    }

    render(){        
        return(
            <div>
                <h2>
                    {this.state.componentState1+' ' +this.state.componentState2+' ' + this.state.componentState3}
                </h2>
                 <form>
                 {/* onChange={this.changeTitle}  */}
                     <input type='text' ref='myInput1'/>
                     <input type='text' ref='myInput2'/>
                     <input type='text' ref='myInput3'/>                  
                     <button type='submit' onClick={this.changeTitleButton}>Send</button>
                 </form>
                 <Cars model={this.state.componentState1} type={this.state.componentState2} color={this.state.componentState3}/>
            </div>
        )
    };
};
export default InputComponents