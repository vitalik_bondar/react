import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/Header';
import Aside from './components/Aside';
import Content from './components/Content';
import Footer from './components/Footer';
import '../src/index.css';
import '../src/bootstrap.min.css';

class Main extends React.Component{
    render(){
        return(
            <div>
                <Header />
                <Content />
                <Footer />
            </div>
        );
    }
}

ReactDOM.render(<Main />, document.getElementById('root'));
