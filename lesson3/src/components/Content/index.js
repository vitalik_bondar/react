import React from 'react';
import Posts from '../Posts/Posts';
import Aside from '../../components/Aside';
import '../../bootstrap.min.css';
import '../Content/content.css';

class Header extends React.Component{
    state={
        title:"nothing",
        posts:[
            {
                title:"Odessa",
                text:"dsdsds"
            },
            {
                title:"Riwne",
                text:"dsdsds"
            },
            {
                title:"Lviv",
                text:"dsdsds"
            },
            
        ],
    };
    changeTitleHandler = (newState) => {
        this.setState({ title: newState })
    };
    render(){
        return(
            <div>
                <div className="container-fluid">
                    <div className="container">
                        <div className="row">
                            <div className="col-9">
                                {this.state.posts.map((i)=>{
                                    return (<Posts Title={i.title} Text={i.text} changeTitle={this.changeTitleHandler.bind(this, i.title)}/>)
                                })}
                                {/* <Posts Title={'Odessa'} Text={'Odessa (also known as Odesa; Ukrainian: Оде́са [ɔˈdɛsɐ]; Russian: Оде́сса ' +
                                '[ɐˈdʲesə]; Yiddish: אַדעס‎) is the third most populous city of Ukraine and a major tourism center, seaport' +
                                ' and transportation hub located on the northwestern shore of the Black Sea. It is also the administrative' +
                                ' center of the Odessa Oblast and a multiethnic cultural center. Odessa is sometimes called the "pearl ' +
                                'of the Black Sea",[2] the "South Capital" (under the Russian Empire and Soviet Union), and "Southern' +
                                ' Palmyra".'} Date={"12.11.2018"}  changeTitle={this.changeTitleHandler.bind(this, 'Odessa')}/>
                                <Posts Title={'Lviv'} Text={'Lviv (Ukrainian: Львів [lʲʋiu̯] (About this sound listen); Russian: Львов' +
                                ' Lvov [lʲvof]; Polish: Lwów [lvuf] (About this sound listen); German: Lemberg; Latin: Leopolis; see ' +
                                'also other names) is the largest city in western Ukraine and the seventh-largest city in the country ' +
                                'overall, with a population of around 728,350 as of 2016. Lviv is one of the main cultural centres of ' +
                                'Ukraine.'} Date={"12.11.2018"}  changeTitle={this.changeTitleHandler.bind(this, 'Lviv')}/>
                                <Posts Title={'Rivne'} Text={'Rivne (Ukrainian: Рівне [ˈrʲiu̯nɛ]; Russian: Ровно, translit. Rovno' +
                                ' [ˈrovnə]; Polish: Równe) is a historic city in western Ukraine and the historical region of Volhynia.' +
                                ' It is the administrative center of Rivne Oblast (province), as well as the surrounding Rivne Raion ' +
                                '(district) within the oblast. Administratively, Rivne is incorporated as a city of oblast significance' +
                                ' and does not belong to the raion. Population: 247,356 (2017 est.)'} Date={"12.11.2018"} changeTitle={this.changeTitleHandler.bind(this, 'Rivne')} /> */}


                            </div>
                            <div className="col-3 aside-main">
                                <Aside Title={this.state.title}/>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )
    };
};
export default Header