import React from 'react';
import '../../bootstrap.min.css';
import '../Header/header.css';

class Header extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="container">
                    <header>
                        <nav>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">How its work</a></li>
                            </ul>
                        </nav>
                    </header>
                </div>
            </div>
        )
    };
};
export default Header