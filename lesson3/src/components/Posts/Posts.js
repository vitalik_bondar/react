import React from 'react';
import './/posts.css';
class Posts extends React.Component{
    render(){
        const prop=this.props;
        return(
            <div className="posts">
                <div onClick={this.props.changeTitle}>{prop.Title}</div>
                <div onClick={this.props.changeTitle}>{prop.Text}</div>
                <div onClick={this.props.changeTitle}>{prop.Date}</div>
            </div>
        )
    };
};
export default Posts