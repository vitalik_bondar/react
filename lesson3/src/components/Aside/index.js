import React from 'react';
import '../../bootstrap.min.css';
import '../Aside/aside.css';
class Header extends React.Component{
    render(){
        const prop=this.props;
        return(
            <div className="aside-main">
                <h1>{prop.Title}</h1>
            </div>
        )
    };
};
export default Header